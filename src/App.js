import './App.css';
import Navbar from './Components/Navbar/Navbar';
import Input from './Components/Input/Input';

function App() {

  return (
    <div className="App">
      <Navbar />
      <Input />
    </div>
    
  );
}

export default App;
