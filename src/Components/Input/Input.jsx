import './Input.css'

function Input() {

    return (
      <div className="input" >
        <style>
          @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap');
          @import url('https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap');
        </style>
        <div className="container-input">
          <div className="input-wrapper">
            <div className="input-bar">
              <div className="nama">
                <p>Name</p>
                <input className="form-control" type="text"/>
              </div>
              <div className="email">
                <p>Email</p>
                <input className="form" type="text"/>
              </div>
            </div>
            
            <button className="btn">
              <p>Sign Me Up</p>
            </button>
          </div>
          
        </div>
      </div>
    );
  }
  
export default Input;