import './Navbar.css'

function Navbar() {

    return (
      <div className="nav" >
        <style>
          @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@500&display=swap');
          @import url('https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap');
        </style>
        <div className="container-nav">
          <div className="pic-wrapper">
            <img className="img-wrapper" src= "Picture/bangunan.jpg" alt="gang"/>
          </div>
          <div className="content-wrapper">
            <h1>got marketing? <br></br>advance your business insight.</h1>
            <p>Fill out the form and receive our award winning newsletter.</p>
          </div>
          
        </div>
      </div>
    );
  }
  
export default Navbar;